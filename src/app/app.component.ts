import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component-min.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public seconds:number = 0;
  public miliseconds:number = 0;
  public started:boolean = false;
  public button1_name:string = "start";
  public button2_name:string = "reset";
  interval:number=0;

  ngOnInit():void{
    // this.doCountdown();
  }

  

  constructor(){};

  
  doCountdown(){
    this.interval = window.setInterval(()=>{
      
      if(this.miliseconds < 99){
        this.miliseconds = this.miliseconds + 1;
      }
      else{
        if(this.seconds < 59){
          this.miliseconds = 0;
          this.seconds = this.seconds + 1;
        }
        else{
          console.log("---- stop watch ended ----");
          clearInterval(this.interval);
        }
      }
    },10);
  }

  start(){
    if(this.started){
      this.started = !this.started;
      console.log("---- stop watch ended ----");
      clearInterval(this.interval);
    }
    else{
      this.started = !this.started;
      this.doCountdown();
    }
    this.started ? this.button1_name = "stop" : this.button1_name = "start";
    
  }

  reset(){
    console.log("---- stop watch ended ----");
    clearInterval(this.interval);
    this.miliseconds = 0;
    this.seconds = 0;
    this.started = false;
  }

}
